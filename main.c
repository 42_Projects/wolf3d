/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 14:23:06 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/12 11:14:45 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"
#include <math.h>

int				main(void)
{
	t_mlx		*mlx;
	t_map		*map;

	if (!(mlx = ft_mlx()) ||
		!(mlx->win = ft_win(mlx, "Wolf 3D", PRINCIPAL)) ||
		!(mlx->win->img = ft_img(mlx->win, mlx->win->size, PRINCIPAL)))
		return (1);
	if (!(map = ft_map()))
		return (1);
	if (!(mlx->win->img->map = map))
		return (1);
	ft_draw_map(mlx->win->img, mlx->win->img->map);
	mlx->enable_hooks(mlx, mlx->win);
	mlx->loop(mlx);
	return (0);
}
