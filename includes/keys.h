/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/23 13:48:17 by Belotte           #+#    #+#             */
/*   Updated: 2016/03/17 11:45:06 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYS_H
# define KEYS_H

# define A		0
# define B		11
# define C		8
# define D		2
# define E		14
# define F		3
# define G		5
# define H		4
# define I		34
# define J		38
# define K		40
# define L		37
# define M		46
# define N		45
# define O		31
# define P		35
# define Q		12
# define R		15
# define S		1
# define T		17
# define U		32
# define V		9
# define W		13
# define X		7
# define Y		16
# define Z		6

# define TOP	126
# define BOT	125
# define LEFT	123
# define RIGHT	124

# define MORE	69
# define LESS	78

# define ENTER	36
# define DEL	51

# define ESC	53
# define TAB	48
# define SHIFT	257
# define CTRL	256
# define ALT	261
# define CMD	259

#endif
