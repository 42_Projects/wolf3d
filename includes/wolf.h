/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 10:33:31 by fbellott          #+#    #+#             */
/*   Updated: 2016/05/12 22:58:26 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# include <fcntl.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <time.h>
# include "mlx.h"
# include "keys.h"
# include "ft.h"

# define WIDTH			512
# define HEIGHT			384
# define MAP_WIDTH		30
# define MAP_HEIGHT		30

/*
**	WINDOWS
*/
# define PRINCIPAL		0
# define OPTIONS		1
# define RADAR			2

typedef struct s_mlx	t_mlx;
typedef struct s_win	t_win;
typedef struct s_img	t_img;
typedef struct s_map	t_map;
typedef struct s_color	t_color;
typedef struct s_vector	t_vector;
typedef struct s_keys	t_keys;

static int				g_world_map[MAP_WIDTH][MAP_HEIGHT] =
{
	{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 2, 2, 2, 2, 2, 2, 0, 0, 0, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 4,
		0, 4, 4, 0, 1},
	{1, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 4,
		0, 4, 4, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 4, 4, 4, 0, 4,
		0, 4, 4, 0, 1},
	{1, 0, 0, 5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 4, 0, 4,
		0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 0, 4, 0, 4,
		4, 4, 4, 0, 1},
	{1, 0, 5, 0, 5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 4, 0, 0,
		0, 4, 4, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 4, 4, 4, 0, 4,
		0, 4, 4, 0, 1},
	{1, 0, 5, 0, 5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 4, 4, 0, 0, 4,
		0, 4, 4, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4,
		0, 4, 4, 0, 1},
	{1, 0, 5, 0, 5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 4,
		0, 4, 4, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 1},
	{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1}
};

struct					s_mlx
{
	void				*ptr;
	t_win				*win;
	int					key_pressed;

	int					(*loop)(t_mlx *mlx);
	int					(*key_down)(int key, t_mlx *mlx);
	int					(*key_up)(int key, t_mlx *mlx);
	int					(*mouse_down)(int button, int x, int y, t_mlx *mlx);
	int					(*mouse_up)(int button, int x, int y, t_mlx *mlx);
	int					(*mouse_move)(int x, int y, t_mlx *mlx);
	int					(*expose)(t_mlx *mlx);
	void				(*enable_hooks)(t_mlx *mlx, t_win *win);
	t_win				*(*add_win)(t_mlx *mlx, char *title, int id);
	char				(*remove_win)(t_mlx *mlx, int id);
	t_win				*(*get_win)(t_mlx *mlx, int id);
};

struct					s_win
{
	t_mlx				*mlx;
	void				*ptr;

	t_size				size;
	char				*title;
	int					id;
	t_img				*img;

	void				(*put_img)(t_img *img);

	struct s_win		*next;
};

struct					s_img
{
	t_mlx				*mlx;
	t_win				*win;
	void				*ptr;

	t_size				size;
	t_point				origin;
	int					id;
	t_map				*map;

	char				*data;
	int					bpp;
	int					size_line;
	int					endian;

	struct s_img		*next;
};

struct					s_vector
{
	double				x;
	double				y;
};

struct					s_color
{
	char				r;
	char				g;
	char				b;
	char				a;
};

struct					s_keys
{
	char				a;
	char				w;
	char				s;
	char				d;
	char				top;
	char				bot;
	char				left;
	char				right;
};

struct					s_map
{
	t_size				size;
	t_vector			pos;
	t_vector			dir;
	t_vector			scale;
	t_vector			plane;
	t_vector			ray_pos;
	t_vector			ray_dir;
	t_point				map_pos;
	float				h;
	t_vector			side_dist;
	t_vector			delta_dist;
	double				per_wall_dist;
	float				angle;
	int					speed;
	t_point				step;
	int					hit;
	char				side;
	int					line_height;
	int					draw_start;
	int					draw_end;
	t_color				c;
	t_keys				keys;
};

t_mlx					*ft_mlx(void);
t_win					*ft_win(t_mlx *mlx, char *title, int id);
t_img					*ft_img(t_win *win, t_size size, int id);
t_map					*ft_map(void);

t_win					*ft_add_win(t_mlx *mlx, char *title, int id);
char					ft_remove_win(t_mlx *mlx, int id);
t_win					*ft_get_win(t_mlx *mlx, int id);
t_img					*ft_add_img(t_win *win, t_size size, int id);
void					ft_draw_map(t_img *img, t_map *map);

t_color					ft_color_rgba(char r, char g, char b, char a);
t_color					ft_color_int(int c);

t_vector				ft_vector(double x, double y);

void					ft_clear_win(t_win *win);
void					ft_clear_img(t_img *img);
void					ft_fill_img(t_img *img, t_color color);
void					ft_put_img(t_img *img);

void					ft_move(t_img *img, t_map *map, int key);
void					ft_rotate_right(t_map *m);
void					ft_rotate_left(t_map *m);

void					ft_radar(t_mlx *mlx);

/*
**	drawing
*/
void					ft_draw_point(t_img *img, t_point p, t_color c);
void					ft_draw_line(t_img *img, t_point p1, t_point p2,
							t_color c);

/*
**	loop & hooks
*/
int						ft_mlx_loop(t_mlx *mlx);
int						ft_mlx_loop_hook(t_mlx *mlx);
int						ft_mlx_key_down(int key, t_mlx *mlx);
int						ft_mlx_key_up(int key, t_mlx *mlx);
int						ft_mlx_mouse_down(int button, int x, int y, t_mlx *mlx);
int						ft_mlx_mouse_up(int button, int x, int y, t_mlx *mlx);
int						ft_mlx_mouse_move(int x, int y, t_mlx *mlx);
int						ft_mlx_expose(t_mlx *mlx);
void					ft_mlx_enable_hooks(t_mlx *mlx, t_win *win);

#endif
