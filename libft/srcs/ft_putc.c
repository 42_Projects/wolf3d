/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 21:01:19 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/18 18:50:53 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

char		ft_putc(char c)
{
	return ((int)write(OUTPUT, &c, 1));
}

char		ft_putc_fd(int fd, char c)
{
	return ((int)write(fd, &c, 1));
}
