/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lines.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 10:43:40 by fbellott          #+#    #+#             */
/*   Updated: 2016/05/12 21:54:29 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

t_lines		*ft_lines(char *s)
{
	t_lines	*line;

	if (!(line = (t_lines *)malloc(sizeof(t_lines))))
		return (NULL);
	line->line = ft_sdup(s);
	line->id = 0;
	line->n = 0;
	line->prev = NULL;
	line->next = NULL;
	return (line);
}

t_lines		*ft_add_line(t_lines *lines, char *s)
{
	t_lines	*line;

	if (!(line = lines))
		return ((lines = ft_lines(s)));
	while (line->next)
		line = line->next;
	line->next = ft_lines(s);
	line->prev = line;
	return (lines);
}
