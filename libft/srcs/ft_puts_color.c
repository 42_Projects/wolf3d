/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puts_color.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 23:18:49 by fbellott          #+#    #+#             */
/*   Updated: 2016/01/25 23:41:15 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int			ft_puts_color(char *s, int color)
{
	if (!s)
		return (0);
	ft_puts("\x1b[");
	ft_putn(color);
	ft_putc('m');
	ft_puts(s);
	ft_puts("\x1b[0m");
	return (ft_slen(s));
}
