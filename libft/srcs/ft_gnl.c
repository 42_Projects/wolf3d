/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_next_line.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 10:01:16 by fbellott          #+#    #+#             */
/*   Updated: 2016/02/22 00:35:42 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

static int			pos_of_first_c(const char *s, char c)
{
	int				i;

	i = -1;
	if (!s)
		return (ERROR);
	while (s[++i] && s[i] != c)
		;
	if (!s[i])
		return (ERROR);
	return (i);
}

static t_files		*add_files(t_files **f, int fd)
{
	t_files			*next;
	t_files			*buff;

	if ((buff = *f))
	{
		while (buff->next)
		{
			if (buff->fd == fd)
				return (buff);
			buff = buff->next;
		}
		if (buff->fd == fd)
			return (buff);
	}
	if (!(next = (t_files *)malloc(sizeof(t_files))) ||
		!(next->buff = ft_snew(BUFF_SIZE + 1)) ||
		!(next->rest_of_buff = ft_snew(1)))
		return (NULL);
	next->fd = fd;
	next->eof = FALSE;
	next->next = NULL;
	if (*f && (buff->next = next))
		return (next);
	*f = next;
	return (*f);
}

char				reading(t_files *f)
{
	char			*buff;

	if (!f->eof)
		f->line = ft_sdup(f->rest_of_buff);
	ft_sdel(&f->rest_of_buff);
	f->bytes_readed = -2;
	while (pos_of_first_c(f->line, '\n') < 0 &&
			(f->bytes_readed = read(f->fd, f->buff, BUFF_SIZE)) > 0)
	{
		f->buff[f->bytes_readed] = '\0';
		buff = ft_sdup(f->line);
		ft_sdel(&f->line);
		f->line = ft_sjoin(buff, f->buff);
		ft_sdel(&buff);
	}
	if (f->bytes_readed == ERROR)
		return (ERROR);
	return (TRUE);
}

int					ft_gnl(int const fd, char **line)
{
	static t_files	*files;
	t_files			*f;

	if (!line || BUFF_SIZE < 0 || read(fd, NULL, 0) == -1)
		return (ERROR);
	f = add_files(&files, fd);
	if (reading(f) == ERROR)
		return (ERROR);
	f->pos_of_first_nl = pos_of_first_c(f->line, '\n');
	if (f->pos_of_first_nl >= 0 && !f->eof)
	{
		f->rest_of_buff = ft_sdup(f->line + f->pos_of_first_nl + 1);
		f->buff_ = ft_ssub(f->line, 0, f->pos_of_first_nl);
		ft_sdel(&f->line);
		f->line = ft_sdup(f->buff_);
		ft_sdel(&f->buff_);
	}
	*line = ft_sdup(f->line);
	if ((f->bytes_readed == 0 || f->bytes_readed == -2) &&
			!(f->rest_of_buff) && ft_slen(f->line) == 0)
		return (0);
	if (ft_slen(*line) == 0 && f->eof)
		return (0);
	ft_sdel(&f->line);
	return (TRUE);
}
