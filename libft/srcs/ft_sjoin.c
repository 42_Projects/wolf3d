/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sjoin.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 20:54:50 by fbellott          #+#    #+#             */
/*   Updated: 2016/01/24 20:56:54 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

char		*ft_sjoin(char *s1, char *s2)
{
	char	*s;
	int		i;
	int		j;

	if (!(s = ft_snew(ft_slen(s1) + ft_slen(s2))))
		return (NULL);
	i = -1;
	j = -1;
	while (s1 && s1[++j])
		s[++i] = s1[j];
	j = -1;
	while (s2 && s2[++j])
		s[++i] = s2[j];
	return (s);
}
