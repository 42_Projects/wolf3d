/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puts.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 21:00:30 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/18 18:47:13 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int			ft_puts(char *s)
{
	if (!s)
		return (0);
	return ((int)write(OUTPUT, s, ft_slen(s)));
}

int			ft_puts_fd(int fd, char *s)
{
	if (!s)
		return (0);
	return ((int)write(fd, s, ft_slen(s)));
}
