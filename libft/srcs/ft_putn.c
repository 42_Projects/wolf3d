/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putn.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 22:59:07 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/18 18:46:31 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int			ft_putn(int n)
{
	char	*s;
	int		len;

	if (!(s = ft_itoa(n)))
		return (0);
	len = (int)write(OUTPUT, s, ft_slen(s));
	ft_sdel(&s);
	return (len);
}

int			ft_putn_fd(int fd, int n)
{
	char	*s;
	int		len;

	if (!(s = ft_itoa(n)))
		return (0);
	len = (int)write(fd, s, ft_slen(s));
	ft_sdel(&s);
	return (len);
}
