/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_point.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/23 10:46:06 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/05 12:43:54 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

t_point		ft_point(int x, int y)
{
	t_point	p;

	p.x = x;
	p.y = y;
	p.z = 0;
	return (p);
}

t_point		ft_point_z(int x, int y, int z)
{
	t_point	p;

	p.x = x;
	p.y = y;
	p.z = z;
	return (p);
}

t_point		ft_op_point(char op, t_point p1, t_point p2)
{
	t_point	p;

	if (op == '+' || op == '-')
	{
		p.x = op == '+' ? p1.x + p2.x : p1.x - p2.x;
		p.y = op == '+' ? p1.y + p2.y : p1.y - p2.y;
	}
	else
		p = p1;
	return (p);
}
