/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssplit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 21:03:35 by fbellott          #+#    #+#             */
/*   Updated: 2016/05/12 21:53:36 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

static int	len(char *s, char c)
{
	int		i;
	int		j;

	if (!s)
		return (0);
	i = -1;
	j = 1;
	while (s[++i])
	{
		if (s[i] == c && i > 0 && s[i - 1] != c && s[i + 1])
			j++;
	}
	return (j);
}

static int	len_of_part(char *s, char c)
{
	int		i;

	if (!s)
		return (0);
	i = -1;
	while (s[++i])
	{
		if (s[i] == c)
			return (i);
	}
	return (i);
}

char		**ft_ssplit(char *s, char c)
{
	char	**array;
	int		i;
	t_point	p;

	if (!s || !(array = (char **)malloc(sizeof(char *) * (len(s, c) + 1))))
		return (NULL);
	i = -1;
	while (s[++i] == c)
		;
	p = ft_point(-1, -1);
	array[++p.y] = ft_snew(len_of_part(s + i, c));
	while (s[i])
	{
		array[p.y][++p.x] = s[i];
		if (s[++i] == c)
		{
			array[++p.y] = ft_snew(len_of_part(s + i, c));
			p.x = -1;
			while (s[i] == c && s[i])
				i++;
		}
	}
	array[p.y + (ft_slen(array[p.y]) > 0)] = NULL;
	return (array);
}
