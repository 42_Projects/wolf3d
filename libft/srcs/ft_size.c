/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_size.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/23 10:47:34 by fbellott          #+#    #+#             */
/*   Updated: 2016/01/23 10:48:22 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

t_size		ft_size(int width, int height)
{
	t_size	s;

	s.width = width;
	s.height = height;
	return (s);
}
