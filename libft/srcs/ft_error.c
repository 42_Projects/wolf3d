/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/18 16:20:33 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/17 22:57:24 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void		*ft_error(char *s)
{
	if (s)
		ft_puts_color(s, red);
	return (NULL);
}

int			ft_error_int(char *s)
{
	if (s)
		ft_puts_color(s, red);
	return (0);
}
