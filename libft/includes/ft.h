/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/23 10:28:37 by fbellott          #+#    #+#             */
/*   Updated: 2016/05/12 21:53:51 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_LIGHT_H
# define LIBFT_LIGHT_H

# include <unistd.h>
# include <stdlib.h>
# include <string.h>
# include <stdio.h>

# define INPUT		0
# define OUTPUT		1
# define ERR		2

# define TRUE		1
# define FALSE		0
# define ERROR		-1

# define READ		O_RDONLY
# define WRITE		O_WRONLY
# define RW			O_RDWR

# define BUFF_SIZE	1024

enum				e_color
{
	red = 31,
	green = 32,
	yellow = 33,
	blue = 34,
	magenta = 35,
	cyan = 36,
	reset = 0
};

typedef struct		s_point
{
	int				x;
	int				y;
	int				z;
}					t_point;

typedef struct		s_size
{
	int				width;
	int				height;
}					t_size;

typedef struct		s_lines
{
	char			*line;
	int				id;
	int				n;
	struct s_lines	*prev;
	struct s_lines	*next;
}					t_lines;

typedef struct		s_files
{
	int				fd;
	char			*buff;
	char			*buff_;
	int				eof;
	char			*line;
	char			*rest_of_buff;
	int				bytes_readed;
	int				pos_of_first_nl;
	struct s_files	*next;
}					t_files;

/*
**	STRUCT
*/
t_point				ft_point(int x, int y);
t_point				ft_point_z(int x, int y, int z);
t_point				ft_op_point(char op, t_point p1, t_point p2);
t_size				ft_size(int width, int height);
t_lines				*ft_lines(char *s);
t_lines				*ft_add_line(t_lines *line, char *s);

/*
**	MEMORY
*/

/*
**	TOOLS
*/
void				*ft_error(char *s);
int					ft_error_int(char *s);

/*
**	STRING
*/
char				*ft_snew(int len);
char				*ft_sdup(char *s);
char				*ft_ssub(char *s, unsigned int start, size_t len);
char				*ft_sjoin(char *s1, char *s2);
char				*ft_srev(char *s);
int					ft_ssearch(char *s, char c);
int					ft_slen(char *s);
int					ft_puts(char *s);
int					ft_puts_fd(int fd, char *s);
int					ft_puts_color(char *s, int color);
char				ft_putc(char c);
char				ft_putc_fd(int fd, char c);
char				ft_nl(void);
char				ft_sdel(char **s);
char				**ft_ssplit(char *s, char c);

/*
**	INTEGER
*/
char				*ft_itoa(int n);
int					ft_atoi(char *s);
int					ft_putn(int n);
int					ft_putn_fd(int fd, int n);
int					ft_putn_color(int n, int color);

/*
**	MATH
*/
int					ft_sqrt(int n);
int					ft_pow(int n, int exp);
int					ft_fact(int n);
int					ft_abs(int n);

/*
**	TOOLS
*/
int					ft_gnl(int fd, char **line);

#endif
