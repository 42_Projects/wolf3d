/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/22 09:29:58 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/12 11:20:56 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

t_color			ft_color_rgba(char r, char g, char b, char a)
{
	t_color		c;

	c.r = r;
	c.g = g;
	c.b = b;
	c.a = a;
	return (c);
}

t_color			ft_color_int(int color)
{
	t_color		c;

	c.a = color / (0x1000000);
	color -= c.a * (0x1000000);
	c.b = color / (0x10000);
	color -= c.r * (0x10000);
	c.g = color / (0x100);
	color -= c.g * (0x100);
	c.r = color;
	return (c);
}
