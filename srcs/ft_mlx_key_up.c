/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx_key_up.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 22:09:06 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/12 21:37:28 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int				ft_mlx_key_up(int key, t_mlx *mlx)
{
	t_map		*map;

	if (!mlx || !(map = mlx->win->img->map))
		return (-1);
	mlx->key_pressed = mlx->key_pressed == key ? -1 : mlx->key_pressed;
	map->keys.w = key == W ? FALSE : map->keys.w;
	map->keys.s = key == S ? FALSE : map->keys.s;
	map->keys.d = key == D ? FALSE : map->keys.d;
	map->keys.a = key == A ? FALSE : map->keys.a;
	map->keys.top = key == TOP ? FALSE : map->keys.top;
	map->keys.bot = key == BOT ? FALSE : map->keys.bot;
	map->keys.left = key == LEFT ? FALSE : map->keys.left;
	map->keys.right = key == RIGHT ? FALSE : map->keys.right;
	return (key);
}
