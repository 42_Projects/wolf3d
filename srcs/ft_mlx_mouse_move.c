/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx_mouse_move.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 22:09:06 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/08 14:43:40 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int				ft_mlx_mouse_move(int x, int y, t_mlx *mlx)
{
	if (!mlx)
		return (0);
	(void)x;
	(void)y;
	return (1);
}
