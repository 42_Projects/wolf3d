/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_point.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/22 09:26:18 by Belotte           #+#    #+#             */
/*   Updated: 2016/03/22 19:24:58 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void			ft_draw_point(t_img *img, t_point p, t_color c)
{
	if (!img || p.x < 0 || p.y < 0 ||
		p.x > img->size.width || p.y > img->size.height)
		return ;
	img->data[p.x * 4 + p.y * img->size_line + 0] = c.r;
	img->data[p.x * 4 + p.y * img->size_line + 1] = c.g;
	img->data[p.x * 4 + p.y * img->size_line + 2] = c.b;
	img->data[p.x * 4 + p.y * img->size_line + 3] = c.a;
}
