/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 21:13:18 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/10 17:35:08 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

t_mlx			*ft_mlx(void)
{
	t_mlx		*mlx;

	if (!(mlx = (t_mlx *)malloc(sizeof(t_mlx))) ||
		!(mlx->ptr = mlx_init()))
		return (NULL);
	mlx->win = NULL;
	mlx->key_pressed = -1;
	mlx->loop = (&ft_mlx_loop);
	mlx->key_down = (&ft_mlx_key_down);
	mlx->key_up = (&ft_mlx_key_up);
	mlx->mouse_down = (&ft_mlx_mouse_down);
	mlx->mouse_up = (&ft_mlx_mouse_up);
	mlx->mouse_move = (&ft_mlx_mouse_move);
	mlx->expose = (&ft_mlx_expose);
	mlx->enable_hooks = (&ft_mlx_enable_hooks);
	mlx->add_win = (&ft_add_win);
	mlx->remove_win = (&ft_remove_win);
	mlx->get_win = (&ft_get_win);
	return (mlx);
}

void			ft_mlx_enable_hooks(t_mlx *mlx, t_win *win)
{
	if (!mlx || !win)
		return ;
	mlx_loop_hook(mlx->ptr, ft_mlx_loop_hook, mlx);
	mlx_hook(win->ptr, 2, (1L << 0), mlx->key_down, mlx);
	mlx_hook(win->ptr, 3, (1L << 1), mlx->key_up, mlx);
	mlx_hook(win->ptr, 4, (1L << 2), mlx->mouse_down, mlx);
	mlx_hook(win->ptr, 5, (1L << 3), mlx->mouse_up, mlx);
	mlx_hook(win->ptr, 6, (1L << 4), mlx->mouse_move, mlx);
	mlx_hook(win->ptr, 12, (1L << 0), mlx->expose, mlx);
}
