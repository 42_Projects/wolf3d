/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_img.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/22 18:53:59 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/10 17:35:14 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

t_img			*ft_img(t_win *win, t_size size, int id)
{
	t_img		*img;

	if (!(img = (t_img *)malloc(sizeof(t_img))) ||
		!(img->win = win) ||
		!(img->mlx = win->mlx) ||
		!(img->ptr = mlx_new_image(win->mlx->ptr, size.width, size.height)) ||
		!(img->data = mlx_get_data_addr(img->ptr, &img->bpp, &img->size_line,
			&img->endian)))
		return (NULL);
	img->size = size;
	img->origin = ft_point(0, 0);
	img->id = id;
	img->next = NULL;
	return (img);
}

t_img			*ft_add_img(t_win *win, t_size size, int id)
{
	t_img		*img;

	if (!win || !win->mlx)
		return (NULL);
	if (!(img = win->img))
		return (win->img = ft_img(win, size, id));
	while (img->next)
		img = img->next;
	return ((img->next = ft_img(win, size, id)));
}

void			ft_fill_img(t_img *img, t_color color)
{
	t_point		p;

	if (!img)
		return ;
	p.y = -1;
	while (++p.y < img->size.height)
	{
		p.x = -1;
		while (++p.x < img->size.width)
		{
			img->data[p.x * 4 + p.y * img->size_line + 0] = color.r;
			img->data[p.x * 4 + p.y * img->size_line + 1] = color.g;
			img->data[p.x * 4 + p.y * img->size_line + 2] = color.b;
			img->data[p.x * 4 + p.y * img->size_line + 3] = color.a;
		}
	}
}

void			ft_clear_img(t_img *img)
{
	int			i;
	int			size;

	if (!img)
		return ;
	i = -1;
	size = img->size.height * img->size_line;
	while (++i < size)
		img->data[i] = 0;
}

void			ft_put_img(t_img *img)
{
	if (!img || !img->win || !img->mlx)
		return ;
	mlx_put_image_to_window(img->mlx->ptr, img->win->ptr, img->ptr,
		img->origin.x, img->origin.y);
}
