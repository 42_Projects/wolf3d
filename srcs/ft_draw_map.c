/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 18:17:01 by fbellott          #+#    #+#             */
/*   Updated: 2016/05/12 21:52:04 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void		init_values(t_img *img, t_map *m, double *map_x, int x)
{
	*map_x = 2 * x / (double)img->size.width - 1;
	m->ray_pos = ft_vector(m->pos.x, m->pos.y);
	m->ray_dir = ft_vector(m->dir.x + m->plane.x * *map_x,
								m->dir.y + m->plane.y * *map_x);
	m->map_pos = ft_point(m->ray_pos.x, m->ray_pos.y);
	m->delta_dist = ft_vector(
				sqrt(1 + pow(m->ray_dir.y, 2) / pow(m->ray_dir.x, 2)),
				sqrt(1 + pow(m->ray_dir.x, 2) / pow(m->ray_dir.y, 2)));
	m->hit = 0;
	m->step.x = m->ray_dir.x < 0 ? -1 : 1;
	m->side_dist.x = m->ray_dir.x < 0 ? (m->ray_pos.x - m->map_pos.x) *
		m->delta_dist.x : (m->map_pos.x + 1 - m->ray_pos.x) * m->delta_dist.x;
	m->step.y = m->ray_dir.y < 0 ? -1 : 1;
	m->side_dist.y = m->ray_dir.y < 0 ? (m->ray_pos.y - m->map_pos.y) *
		m->delta_dist.y : (m->map_pos.y + 1 - m->ray_pos.y) * m->delta_dist.y;
}

static void		calcul(t_img *img, t_map *m)
{
	while (!m->hit)
	{
		m->side = !(m->side_dist.x < m->side_dist.y);
		if (!m->side)
			m->side_dist.x += m->delta_dist.x;
		else
			m->side_dist.y += m->delta_dist.y;
		if (!m->side)
			m->map_pos.x += m->step.x;
		else
			m->map_pos.y += m->step.y;
		if (g_world_map[m->map_pos.x][m->map_pos.y] > 0)
			m->hit = 1;
	}
	m->per_wall_dist = !m->side ?
		(m->map_pos.x - m->ray_pos.x + (1 - m->step.x) / 2) / m->ray_dir.x :
		(m->map_pos.y - m->ray_pos.y + (1 - m->step.y) / 2) / m->ray_dir.y;
	m->line_height = img->size.height / m->per_wall_dist;
	m->draw_start = -m->line_height / 2 + img->size.height / 2 + m->h;
	m->draw_start = m->draw_start < 0 ? 0 : m->draw_start;
	m->draw_end = m->line_height / 2 + img->size.height / 2 + m->h;
	m->draw_end = m->draw_end >= img->size.height ? img->size.height - 1 :
													m->draw_end;
}

void			ft_draw_map(t_img *img, t_map *m)
{
	int			x;
	double		map_x;
	int			c;

	if (!img || !m || !(x = -1))
		return ;
	m->size = ft_size(MAP_WIDTH, MAP_HEIGHT);
	while (++x < img->size.width)
	{
		init_values(img, m, &map_x, x);
		calcul(img, m);
		c = 0x000000;
		c = g_world_map[m->map_pos.x][m->map_pos.y] == 1 ? 0xA65041 : c;
		c = g_world_map[m->map_pos.x][m->map_pos.y] == 2 ? 0xA67541 : c;
		c = g_world_map[m->map_pos.x][m->map_pos.y] == 3 ? 0x2B5968 : c;
		c = g_world_map[m->map_pos.x][m->map_pos.y] >= 4 ? 0x2F7A49 : c;
		c /= m->side == 1 ? 2 : 1;
		ft_draw_line(img, ft_point(x, m->draw_start),
						ft_point(x, m->draw_end), ft_color_int(c));
		ft_draw_line(img, ft_point(x, m->draw_end), ft_point(x, HEIGHT - 1),
						ft_color_int(0x282828));
	}
	ft_radar(img->mlx);
	img->win->put_img(img);
}
