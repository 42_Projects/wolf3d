/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx_mouse_up.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 22:09:06 by Belotte           #+#    #+#             */
/*   Updated: 2016/03/16 22:18:25 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int				ft_mlx_mouse_up(int button, int x, int y, t_mlx *mlx)
{
	if (!mlx)
		return (0);
	(void)x;
	(void)y;
	return (button);
}
