/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/10 18:02:31 by fbellott          #+#    #+#             */
/*   Updated: 2016/05/10 18:02:50 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		ft_rotate_right(t_map *m)
{
	double	buff;

	buff = m->dir.x;
	m->dir.x = m->dir.x * cos(-m->angle) - m->dir.y * sin(-m->angle);
	m->dir.y = buff * sin(-m->angle) + m->dir.y * cos(-m->angle);
	buff = m->plane.x;
	m->plane.x = m->plane.x * cos(-m->angle) - m->plane.y * sin(-m->angle);
	m->plane.y = buff * sin(-m->angle) + m->plane.y * cos(-m->angle);
}

void		ft_rotate_left(t_map *m)
{
	double	buff;

	buff = m->dir.x;
	m->dir.x = m->dir.x * cos(m->angle) - m->dir.y * sin(m->angle);
	m->dir.y = buff * sin(m->angle) + m->dir.y * cos(m->angle);
	buff = m->plane.x;
	m->plane.x = m->plane.x * cos(m->angle) - m->plane.y * sin(m->angle);
	m->plane.y = buff * sin(m->angle) + m->plane.y * cos(m->angle);
}
