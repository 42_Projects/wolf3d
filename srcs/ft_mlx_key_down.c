/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx_key_down.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 22:09:06 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/12 21:37:25 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int				ft_mlx_key_down(int key, t_mlx *mlx)
{
	t_map		*map;

	if (!mlx || !(map = mlx->win->img->map))
		return (-1);
	mlx->key_pressed = mlx->key_pressed == -1 ? key : mlx->key_pressed;
	if (key == ENTER)
	{
		mlx->enable_hooks(mlx, mlx->add_win(mlx, "Options", 1));
	}
	if (key == DEL || (key == W && mlx->key_pressed == CMD))
	{
		mlx->remove_win(mlx, mlx->win->next ? 1 : 0);
		return (key);
	}
	if (key == ESC || (key == Q && mlx->key_pressed == CMD))
		exit(0);
	map->keys.w = key == W ? TRUE : map->keys.w;
	map->keys.s = key == S ? TRUE : map->keys.s;
	map->keys.d = key == D ? TRUE : map->keys.d;
	map->keys.a = key == A ? TRUE : map->keys.a;
	map->keys.top = key == TOP ? TRUE : map->keys.top;
	map->keys.bot = key == BOT ? TRUE : map->keys.bot;
	map->keys.left = key == LEFT ? TRUE : map->keys.left;
	map->keys.right = key == RIGHT ? TRUE : map->keys.right;
	return (key);
}
