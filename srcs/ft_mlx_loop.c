/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx_loop.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 21:53:55 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/12 21:51:16 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int				ft_mlx_loop(t_mlx *mlx)
{
	if (!mlx)
		return (-1);
	return (mlx_loop(mlx->ptr));
}

static int		ft_mlx_loop_hook_bis(t_map *map)
{
	char		flag;

	flag = FALSE;
	if (map->keys.top && (flag = TRUE))
		map->h += map->h < 150 ? 8 : 0;
	if (map->keys.bot && (flag = TRUE))
		map->h -= map->h > -150 ? 8 : 0;
	return (flag);
}

int				ft_mlx_loop_hook(t_mlx *mlx)
{
	t_map		*map;
	char		flag;

	if (!mlx || !(map = mlx->win->img->map))
		return (1);
	flag = FALSE;
	if (map->keys.d && (flag = TRUE))
		ft_move(mlx->win->img, mlx->win->img->map, D);
	if (map->keys.a && (flag = TRUE))
		ft_move(mlx->win->img, mlx->win->img->map, A);
	if (map->keys.w && (flag = TRUE))
		ft_move(mlx->win->img, mlx->win->img->map, W);
	if (map->keys.s && (flag = TRUE))
		ft_move(mlx->win->img, mlx->win->img->map, S);
	if (map->keys.left && (flag = TRUE))
		ft_move(mlx->win->img, mlx->win->img->map, LEFT);
	if (map->keys.right && (flag = TRUE))
		ft_move(mlx->win->img, mlx->win->img->map, RIGHT);
	if (!(flag | ft_mlx_loop_hook_bis(map)))
		return (1);
	ft_clear_img(mlx->win->img);
	ft_draw_map(mlx->win->img, mlx->win->img->map);
	return (1);
}
