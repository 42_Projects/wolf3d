/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/22 18:38:35 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/10 17:32:21 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void			ft_draw_line(t_img *img, t_point p1, t_point p2, t_color c)
{
	t_point		d;
	t_point		s;
	int			err;
	int			err_;
	int			i;

	if ((i = -1) && !img)
		return ;
	d = ft_point(ft_abs(p2.x - p1.x), ft_abs(p2.y - p1.y));
	s = ft_point(p1.x < p2.x ? 1 : -1, p1.y < p2.y ? 1 : -1);
	err = (d.x > d.y ? d.x : -d.y) / 2;
	while (++i <= HEIGHT && ((err_ = err) || !err_))
	{
		ft_draw_point(img, p1, c);
		if (p1.x == p2.x && p1.y == p2.y)
			return ;
		if (err_ > -d.x)
			err -= d.y;
		if (err_ > -d.x)
			p1.x += s.x;
		if (err_ < d.y)
			err += d.x;
		if (err_ < d.y)
			p1.y += s.y;
	}
}
