/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_radar.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/08 18:46:20 by fbellott          #+#    #+#             */
/*   Updated: 2016/05/10 17:50:12 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

char		ft_is_pos(t_map *map, t_point p, t_point p2)
{
	char	flag;

	flag = ((int)map->pos.y == p.y &&
			(int)((map->pos.y - (int)map->pos.y) * 10) / 2 == p2.y) ||
			((int)map->pos.x == p.x &&
			((int)((map->pos.x - (int)map->pos.x) * 10) / 2 == p2.x));
	return (flag);
}

void		ft_radar_bis(t_mlx *mlx, t_map *map, t_point p)
{
	t_point	p2;

	p2.y = -1;
	while (++p2.y < 5 && (p2.x = -1))
	{
		while (++p2.x < 5)
		{
			if (g_world_map[p.x][p.y] > 0)
				ft_draw_point(mlx->win->img,
					ft_point(p.y * 5 + p2.y, p.x * 5 + p2.x),
					ft_color_int(0x222222 * g_world_map[p.x][p.y]));
			if (ft_is_pos(map, p, p2))
				ft_draw_point(mlx->win->img,
					ft_point(p.y * 5 + p2.y, p.x * 5 + p2.x),
					ft_color_int(0x282828));
		}
	}
}

void		ft_radar(t_mlx *mlx)
{
	t_map	*map;
	t_img	*img;
	t_size	size;
	t_point	p;

	if (!mlx || !(map = mlx->win->img->map))
		return ;
	img = mlx->win->img;
	size = mlx->win->img->map->size;
	p.y = -1;
	while (++p.y < size.height && (p.x = -1))
	{
		while (++p.x < size.width)
			ft_radar_bis(mlx, map, p);
	}
}
