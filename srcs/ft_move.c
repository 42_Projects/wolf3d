/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 15:32:04 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/12 23:00:08 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void		top(t_map *m, float s)
{
	if (g_world_map[(int)(m->pos.x + m->dir.x / s * 1.1)][(int)m->pos.y] == 0)
		m->pos.x += m->dir.x / s;
	if (g_world_map[(int)m->pos.x][(int)(m->pos.y + m->dir.y / s * 1.1)] == 0)
		m->pos.y += m->dir.y / s;
}

static void		bot(t_map *m, float s)
{
	if (g_world_map[(int)(m->pos.x - m->dir.x / s * 1.1)][(int)m->pos.y] == 0)
		m->pos.x -= m->dir.x / s;
	if (g_world_map[(int)m->pos.x][(int)(m->pos.y - m->dir.y / s * 1.1)] == 0)
		m->pos.y -= m->dir.y / s;
}

static void		left(t_map *m, float s)
{
	if (g_world_map[(int)(m->pos.x - m->dir.y / s * 1.1)][(int)m->pos.y] == 0)
		m->pos.x -= m->dir.y / s;
	if (g_world_map[(int)m->pos.x][(int)(m->pos.y + m->dir.x / s * 1.1)] == 0)
		m->pos.y += m->dir.x / s;
}

static void		right(t_map *m, float s)
{
	if (g_world_map[(int)(m->pos.x + m->dir.y / s * 1.1)][(int)m->pos.y] == 0)
		m->pos.x += m->dir.y / s;
	if (g_world_map[(int)m->pos.x][(int)(m->pos.y - m->dir.x / s * 1.1)] == 0)
		m->pos.y -= m->dir.x / s;
}

void			ft_move(t_img *img, t_map *map, int key)
{
	float		speed;

	if (!img || !map)
		return ;
	if (!(speed = map->speed) && !(ft_error("Speed must be > 0.\n")))
		exit(1);
	if (key == W)
		top(map, speed);
	else if (key == S)
		bot(map, speed);
	else if (key == A)
		left(map, speed);
	else if (key == D)
		right(map, speed);
	else if (key == LEFT)
		ft_rotate_left(map);
	else if (key == RIGHT)
		ft_rotate_right(map);
}
