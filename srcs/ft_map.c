/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 18:08:55 by fbellott          #+#    #+#             */
/*   Updated: 2016/05/12 21:45:38 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

t_map			*ft_map(void)
{
	t_map		*map;

	if (!(map = (t_map *)malloc(sizeof(t_map))))
		return (NULL);
	map->size = ft_size(24, 24);
	map->pos = ft_vector(15, 15);
	map->dir = ft_vector(-1, 0);
	map->plane = ft_vector(0, 2.0 / 3.0);
	map->angle = 0.05;
	map->speed = 5;
	map->h = 0;
	map->keys.a = FALSE;
	map->keys.w = FALSE;
	map->keys.s = FALSE;
	map->keys.d = FALSE;
	map->keys.left = FALSE;
	map->keys.right = FALSE;
	map->keys.top = FALSE;
	map->keys.bot = FALSE;
	return (map);
}
