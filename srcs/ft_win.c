/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_win.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 21:59:28 by Belotte           #+#    #+#             */
/*   Updated: 2016/05/10 17:36:35 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

t_win			*ft_win(t_mlx *mlx, char *title, int id)
{
	t_win		*win;

	if (!mlx || !title ||
		!(win = (t_win *)malloc(sizeof(t_win))) ||
		!(win->ptr = mlx_new_window(mlx->ptr, WIDTH, HEIGHT, title)))
		return (NULL);
	win->id = id;
	win->size = ft_size(WIDTH, HEIGHT);
	win->mlx = mlx;
	win->title = ft_sdup(title);
	win->put_img = (&ft_put_img);
	win->img = NULL;
	win->next = NULL;
	return (win);
}

t_win			*ft_add_win(t_mlx *mlx, char *title, int id)
{
	t_win		*win;

	if (!mlx)
		return (NULL);
	if (!(win = mlx->win))
		return ((mlx->win = ft_win(mlx, title, id)));
	while (win->next)
		win = win->next;
	return ((win->next = ft_win(mlx, title, id)));
}

char			ft_remove_win(t_mlx *mlx, int id)
{
	t_win		*win;
	t_win		*buff;

	if (!mlx || !(win = mlx->win))
		return (FALSE);
	while (win)
	{
		if (win->next && win->next->id == id)
		{
			buff = win->next;
			mlx_destroy_window(mlx->ptr, win->next->ptr);
			win->next = win->next->next;
		}
		else
			win = win->next;
	}
	if (mlx->win->id == id)
	{
		buff = mlx->win->next;
		mlx_destroy_window(mlx->ptr, mlx->win->ptr);
		mlx->win = mlx->win->next;
		if (!mlx->win)
			exit(0);
	}
	return (TRUE);
}

t_win			*ft_get_win(t_mlx *mlx, int id)
{
	t_win		*win;

	if (!mlx || !(win = mlx->win))
		return (NULL);
	while (win)
	{
		if (win->id == id)
			return (win);
		win = win->next;
	}
	return (NULL);
}

void			ft_clear_win(t_win *win)
{
	if (!win)
		return ;
	mlx_clear_window(win->mlx->ptr, win->ptr);
}
